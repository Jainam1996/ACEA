import csv
import random
from flask import Flask, render_template, request, redirect, url_for,Blueprint

#row number- course
number_of_c = 2000
#coloumn number - recommendations of row number
number_of_rc = 2000


index_blueprint = Blueprint('model', __name__)




def random_colour_generation():
	colour = ['aqua', 'green', 'yellow', 'red']
	secure_random = random.SystemRandom()
	return secure_random.choice(colour)


def generate_recommendation():
    with open("output_new.csv") as csvfile:
        reader = csv.reader(csvfile)

        #for course info

        # a list to store all the course names
        column_header = next(reader)

        # a matrix to store the binary data for course and the corresponding recommended courses
        binary_data = [row for row in reader]

        
        # print(column_header)
        # print(binary_data)

        # recommendation list
        rec_list = []

        # temporary list that will be appended to to rec_list after it is updated for each user
        temp_list = []
        # for loop for course i.e. rows
        for i in range(0, number_of_c):
            temp_list.append(i + 1)
            

            # for loop for reccomended courses i.e. columns
            for j in range(1, number_of_rc):
                if binary_data[i][j] == '1':
                    temp_list.append(column_header[j])
                    #ourse_info.append([binary[int(column_header[j])][5],[binary[int(column_header[j])][6]])
                    
            rec_list.append(temp_list)
            temp_list = []
        

    return rec_list




@index_blueprint.route('/')
@index_blueprint.route('/index/')
@index_blueprint.route('/home/')
def index():
		return render_template('new_index.html')

@index_blueprint.route('/new_index/')
def new_index():
    return render_template('index_form.html')
		
@index_blueprint.route('/check_user_id/', methods=['POST'])
def check_user_id():

	# function to render the page only with user specific recommendations
    course_id = int(request.form['course_id'])
    number_of_rc = int(request.form['number_of_rc'])
    final_rec_list = generate_recommendation()
	# user specific recommendation list is present in serial order of the course_id
    if course_id >= 1:
	    course_rec_list = final_rec_list[course_id-1]
	
    return render_template('course_recommendation_page.html', course_rec_list = course_rec_list, colour = random_colour_generation(), course_id = course_id, number_of_rc = number_of_rc) 
	
	
@index_blueprint.route('/test/',methods=['POST'])
def test():
    rec_id=int(request.form['rec_id'])
    with open("dyce.csv") as csvfile:
        reader = csv.reader(csvfile)
        binary_data = [row for row in reader]
        rec_info=[binary_data[rec_id][5],binary_data[rec_id][8]]
    return render_template('course_info.html',rec_info=rec_info)






@index_blueprint.route('/recommendations_page/')
def recommendations_page():
    # function to render the page with all the recommendations
    final_rec_list = generate_recommendation()
	
	# change the for loop range which generates the small box for courses depending upon the number of courses
    return render_template('recommendation_page.html', final_rec_list = final_rec_list, colour = random_colour_generation())
	
	
	
