from flask import Flask
from model.app2 import index_blueprint

application = Flask(__name__)
application.register_blueprint(index_blueprint)

#eceng_blueprint = Blueprint('receng_blueprint', __name__)   # only including this one for now
# receng_blueprint = Blueprint('dyce_blueprint', __name__)
# receng_blueprint = Blueprint('libo_blueprint', __name__)

#admin = Blueprint('receng_blueprint', __name__, template_folder='/Users/jainam/Desktop/FlaskApp-Jainam/FlaskApp/templates')   # temporary folder before we set up separate blueprints as below

# admin = Blueprint('receng_blueprint', __name__, template_folder='/home/scrapers/ACEA/ACEA-Scraping/FlaskApp/receng/templates')
# admin = Blueprint('libo_blueprint', __name__, template_folder='/home/scrapers/ACEA/ACEA-Scraping/FlaskApp/libo/templates')
# admin = Blueprint('dyce_blueprint', __name__, template_folder='/home/scrapers/ACEA/ACEA-Scraping/FlaskApp/dyce/templates')

if __name__ == '__main__':
    application.run(debug = True,host='0.0.0.0',port=80)
